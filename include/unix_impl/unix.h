#pragma once

#include "lua.hh"

void pd2_log(const char* message, int level, const char* file, int line); // Defined in unix.h
#define PD2HOOK_LOG_LEVEL(msg, level, file, line, ...) pd2_log(msg, (int) level, file, line)
