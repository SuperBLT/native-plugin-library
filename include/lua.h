#pragma once

#ifdef _WIN32

// Define the Lua function pointers
#undef INIT_FUNC
#include "sblt_msw32_impl/lua_macros.h"
#include "sblt_msw32_impl/misc.h"
#include "sblt_msw32_impl/fptrs.h"
#include "sblt_msw32_impl/lauxlib.h"

#elif defined(__GNUC__)

#include "unix_impl/unix.h"

#else

#error Unsupported platform

#endif

